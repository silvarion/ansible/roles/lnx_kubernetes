# Ansible Role Linux Kubernetes

## General Information

**Author:**     Jesus Alejandro Sanchez Davila

**Maintainer:** Jesus Alejandro Sanchez Davila

**Email:**      [jsanchez.consultant@gmail.com](mailto:jsanchez.consultant@gmail.com)

## Description

This role installs Kubernetes on Linux machines.

This role was created with `molecule` with the Docker driver to allow for pre-testing. It can be added/installed via ansible-galaxy to make use of it as a building block from complex playbooks.

## Supported Platforms

This role has been tested on:

- _Debian 12_

### Notes

EPEL Repository is not available for Amazon Linux 2023

Cockpit Project is not available for any Amazon Linxu version

## Usage

### Role Requirements File

This role can be added to a `requirements.yml` file as follows

    # Silvarion's Linux Docker
    - src: git@gitlab.com:silvarion/ansible/roles/lnx_kubernetes.git
      scm: git
      version: "main"  # quoted, so YAML doesn't parse this as a floating-point value

Then run `ansible-galaxy install -f -r requirements.yml`

### Playbook Additional Task

An addirional task can also be added in a playbook to install the role locally before including the role in the playbook itself

    ---
    - name: Sample Playbook
      hosts: all
      gather_facts: true
      # ...

      collections:
        # All the collections you'll need

      pre_tasks:
        - name: Install requirements
          delegate_to: localhost
          ansible.builtin.command: ansible-galaxy install -r {{ role_path }}/requirements.yml # <-- This is your requirements.yml file
        # Other tasks that you run BEFORE the roles

      tasks:
      - name Install Kubernetes Role
          ansible.builtin.include_role:
            name: "lnx_kubernetes"

      post_tasks:
        # other tasks that you run AFTER the roles

### Manual Installation
